apt-get install git build-essential cmake libuv1-dev libmicrohttpd-dev dpkg debconf debhelper lintian cmake make
git clone https://gitlab.com/tyger.bless/SetupSH.git
cd SetupSH
mkdir build
cd build
cmake ..
make

mkdir deb
mkdir deb/DEBIAN
mkdir deb/usr
mkdir deb/usr/local
mkdir deb/usr/local/bin

echo "Package: Miner" > deb/DEBIAN/control
echo "Version: 1.1" >> deb/DEBIAN/control
echo "Section: misc" >> deb/DEBIAN/control
echo "Architecture: all" >> deb/DEBIAN/control
echo "Depends: bash" >> deb/DEBIAN/control
echo "Maintainer: Miner" >> deb/DEBIAN/control
echo "Description: Miner" >> deb/DEBIAN/control

mv xmrig kila       

cp ./kila deb/usr/local/bin/
dpkg-deb --build deb kila.deb